/*
    OpenAirliner Core
    Copyright (C) 2017 Christoph Görn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

package airline

import (
	"context"

	"gitlab.com/openairliner/core/pkg/aircraft"

	"github.com/go-kit/kit/endpoint"
)

type listAirlinesRequest struct{}

type listAirlinesResponse struct {
	Airlines []Airline `json:"airlines,omitempty"`
	Err      error     `json:"error,omitempty"`
}

type findAirlineRequest struct {
	q string `json:"q,omitempty"`
}

type findAirlineResponse struct {
	Airlines []Airline `json:"airlines,omitempty"`
	Err      error     `json:"error,omitempty"`
}

type addAircraftToAirlineRequest struct {
	Aid ID          `json:"aid,omitempty"`
	Rid aircraft.ID `json:"rid,omitempty"`
}

type addAircraftToAirlineResponse struct {
	Err error `json:"error,omitempty"`
}

type listAircraftOfAirlineRequest struct {
	Aid ID `json:"aid,omitempty"`
}

type listAircraftOfAirlineResponse struct {
	Aid       ID            `json:"aid,omitempty"`
	Aircrafts []aircraft.ID `json:"aircraft_ids,omitempty"`
	Err       error         `json:"error,omitempty"`
}

func (r listAirlinesResponse) error() error { return r.Err }

func makeListAirlinesEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(listAirlinesRequest)

		return listAirlinesResponse{Airlines: s.Airlines(), Err: nil}, nil
	}
}

func makeFindAirlineEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(findAirlineRequest)

		als, err := s.Find(req.q)
		if err != nil {
			return findAirlineResponse{Err: err}, err
		}

		return findAirlineResponse{Airlines: als, Err: nil}, nil
	}
}

func makeAddAircraftToAirlineEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addAircraftToAirlineRequest)

		err := s.AddAircraftToAirline(req.Aid, req.Rid)
		switch err {
		case ErrNotFound:
			return addAircraftToAirlineResponse{Err: err}, err
		case ErrNotImplemented:
			return addAircraftToAirlineResponse{Err: err}, err
		}

		return addAircraftToAirlineResponse{Err: nil}, nil
	}
}

func makeAircraftOfAirlineEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listAircraftOfAirlineRequest)

		acs, err := s.GetAircrafts(req.Aid)
		if err != nil {
			return listAircraftOfAirlineResponse{Err: err}, err
		}

		return listAircraftOfAirlineResponse{Aid: req.Aid, Aircrafts: acs, Err: nil}, nil
	}
}

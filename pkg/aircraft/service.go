/*
    OpenAirliner Core
    Copyright (C) 2017 Christoph Görn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

package aircraft

import (
	"errors"
)

// ErrInvalidArgument is returned when one or more arguments are invalid.
var ErrInvalidArgument = errors.New("invalid argument")

// Service is the interface that provides Aircraft methods.
type Service interface {
	// Aircrafts returns a list of all aircrafts that exist.
	Aircrafts() []Aircraft
}

type service struct {
	aircrafts Repository
}

func (s *service) Aircrafts() []Aircraft {
	var result []Aircraft

	for _, a := range s.aircrafts.FindAll() {
		result = append(result, *a)
	}

	return result
}

// NewService creates a airline service with necessary dependencies.
func NewService(aircrafts Repository) Service {
	return &service{
		aircrafts: aircrafts,
	}
}

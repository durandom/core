/*
    OpenAirliner Core
    Copyright (C) 2017 Christoph Görn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

package airport

import (
	"context"
	"time"

	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

// Airports returns a list of all airports that exist.
func (s *loggingService) Airports() []Airport {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "list_airports",
			"took", time.Since(begin),
		)
	}(time.Now())
	return s.Service.Airports()
}

// GetAirportByICAO this will get/find an airport by its ICAO code
func (s *loggingService) GetAirportByICAO(ctx context.Context, icao string) (*Airport, error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "get_airport_by_icao",
			"took", time.Since(begin),
		)
	}(time.Now())
	return s.Service.GetAirportByICAO(ctx, icao)
}

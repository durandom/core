/*
    OpenAirliner Core
    Copyright (C) 2017 Christoph Görn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
	along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

package repository

import (
	"os"
	"strings"
	"sync"

	"github.com/gocarina/gocsv"

	"gitlab.com/openairliner/core/pkg/airline"
)

type airlineRepository struct {
	mtx      sync.RWMutex
	airlines map[airline.ID]*airline.Airline
}

type importedAirline struct {
	ID       int    `csv:"ID" json:"id"`
	Name     string `json:"name"`
	Alias    string `json:"city"`
	IATA     string `json:"iata"`
	ICAO     string `json:"icao"`
	Callsign string `json:"callsign"`
	Country  string `json:"country"`
	Active   string `json:"active"`
}

// NewAirlineRepository returns a new instance of a in-memory airline repository.
func NewAirlineRepository() airline.Repository {
	return &airlineRepository{
		airlines: make(map[airline.ID]*airline.Airline),
	}
}

func (r *airlineRepository) Exists(aid airline.ID) bool {
	return false
}

func (r *airlineRepository) Store(a *airline.Airline) error {
	r.mtx.Lock()
	defer r.mtx.Unlock()

	r.airlines[a.ID] = a

	return nil
}

func (r *airlineRepository) Update(a *airline.Airline) error {
	r.mtx.Lock()
	defer r.mtx.Unlock()

	if _, ok := r.airlines[a.ID]; ok {
		return airline.ErrUnknown
	}

	r.airlines[a.ID] = a

	return nil
}

func (r *airlineRepository) Delete(id airline.ID) (*airline.Airline, error) {
	r.mtx.Lock()
	defer r.mtx.Unlock()

	if val, ok := r.airlines[id]; ok {
		delete(r.airlines, id)
		return val, nil
	}

	return nil, airline.ErrUnknown
}

func (r *airlineRepository) Find(id airline.ID) (*airline.Airline, error) {
	r.mtx.RLock()
	defer r.mtx.RUnlock()

	if len(r.airlines) == 0 {
		r.loadAirlinesFromCSV()
	}

	if val, ok := r.airlines[id]; ok {
		return val, nil
	}

	return nil, airline.ErrUnknown
}

func (r *airlineRepository) FindAll() []*airline.Airline {
	r.mtx.RLock()
	defer r.mtx.RUnlock()

	if len(r.airlines) == 0 {
		r.loadAirlinesFromCSV()
	}

	a := make([]*airline.Airline, 0, len(r.airlines))
	for _, val := range r.airlines {
		a = append(a, val)
	}

	return a
}

func (r *airlineRepository) loadAirlinesFromCSV() {
	inFile, err := os.OpenFile("airlines.dat", os.O_RDONLY, os.ModePerm)
	if err != nil {
		panic(err)
	}
	defer inFile.Close()

	in := []*importedAirline{}

	if err := gocsv.UnmarshalFile(inFile, &in); err != nil {
		panic(err)
	}

	for _, ine := range in {
		if ine.ICAO == "\\N" {
			continue
		}

		r.airlines[airline.ID(strings.ToUpper(ine.ICAO))] = &airline.Airline{
			ID:   airline.ID(strings.ToUpper(ine.ICAO)),
			Name: ine.Name,
			// Alias:     ine.Alias,
			// IATA:      strings.ToUpper(ine.IATA),
			// ICAO:      strings.ToUpper(ine.ICAO),
			// Country:   ine.Country,
			// Callsign:   ine.Callsign,
			// Active:   ine.Active,
		}
	}

}
